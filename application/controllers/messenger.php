<?php

class Messenger extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function send_message() {


        $to = $this->input->post('to');
        $message = $this->input->post('message');
        $subject = $this->input->post('subject');
        $headers = 'From: ' . 'mc65799@gmail.com';

        mail($to, $subject, $message, $headers);

        var_dump($this->input->post());
    }

    public function compose() {

        if (!$this->session->userdata('admin_email')) {
            redirect('authentication/login');
        }

        $this->form_validation->set_rules('to', 'To', 'required');

        if ($this->form_validation->run() == FALSE) {

            $this->load->view('templates/header');
            $this->load->view('templates/admin_sidebar');
            $this->load->view('composer');
            $this->load->view('templates/footer');
        } else {

            $to = $this->input->post('to');
            $message = $this->input->post('message');
            $subject = $this->input->post('subject');
            $headers = 'From: ' . $this->input->post('from');

            mail($to, $subject, $message, $headers);
        }
    }

    public function notify_contest_teams() {

        if (!$this->session->userdata('admin_email')) {
            redirect('authentication/login');
        }

        $this->load->model('bsadd_model');
        $this->form_validation->set_rules('contest', 'Contest Name', 'required');

        if ($this->form_validation->run() == FALSE) {

            $this->load->view('templates/header');
            $this->load->view('templates/admin_sidebar', array(
                'contests' => $this->bsadd_model->get_contests()
            ));
            $this->load->view('mail_contest_teams');
            $this->load->view('templates/footer');
        } else {

            $data = array();
            if ($this->input->post('contest', true) != 'project_show') {
                // TODO: fetch and maintain bsadd_contests tables
                $contest_id = $this->bsadd_model->get_contest_id($this->input->post('contest', true));
                $teams = $this->bsadd_model->get_registered_teams($contest_id);

                unset($teams['field']);

                foreach ($teams as $team) {
                    $team_members = $this->bsadd_model->get_team_members($contest_id);

                    foreach ($team_members as $member) {

                        if ($member['email']) {
                            $data[] = array(
                                'email' => $member['email'],
                            );
                        }
                    }
                }
            } else {
                // TODO: fetch and maintain project_show tables

                $this->load->model('project_show_model');
                $projects = $this->project_show_model->get_valid_registered_teams();

                unset($projects['field']);

                foreach ($projects as $project) {
                    $project_members = $this->project_show_model->get_project_members($project['project_id']);

                    foreach ($project_members as $member) {

                        if ($member['email']) {
                            $data[] = array(
                                'email' => $member['email'],
                            );
                        }
                    }
                }
            }

            $this->session->set_userdata('receivers', $data);
            $this->session->set_userdata('subject', $this->input->post('subject', true));
            $this->session->set_userdata('message', $this->input->post('message'));

            $this->smtp_message_interface();

            $this->session->set_userdata('flash_message_type', 'success');
            $this->session->set_userdata('flash_message', 'Message Successfully Sent');

            redirect('messenger/notify_contest_teams');
        }
    }

    public function test() {

        $this->load->library('email');
        $this->email->from('c_maksud@outlook.com');
        $this->email->to('mc65799@gmail.com');
        $this->email->subject('Demo');
        $this->email->message('Message');
        $this->email->send();
    }

    public function request() {
        $this->session->set_userdata('receivers', array(
            array('email' => 'mc65799@gmail.com'), array('email' => 'c_maksud@outlook.com')
        ));

        $this->session->set_userdata('subject', 'localhost');
        $this->session->set_userdata('message', 'Message');

        $this->smtp_message_interface();
    }

    public function smtp_message_interface() {

        $this->config->load('email', FALSE, TRUE);
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com';
        $config['smtp_port'] = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = $this->config->item('smtp_user');
        $config['smtp_pass'] = $this->config->item('smtp_pass');
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
        $config['validation'] = TRUE;

        $this->load->library('email', $config);
        foreach ($this->session->userdata('receivers') as $receiver) {

            $this->email->from($this->config->item('from'));
            $this->email->to($receiver['email']);
            $this->email->subject($this->session->userdata('subject'));
            $this->email->message($this->session->userdata('message'));

            if (!$this->email->send()) {
                // echo $this->email->print_debugger();
            }
        }

        $this->session->unset_userdata('subject');
        $this->session->unset_userdata('message');
        $this->session->unset_userdata('receivers');
    }

}


