<?php

class Project_show extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('project_show_model');
    }

    public function registration() {

        $this->set_rules();
        $this->init_capcha();

        if ($this->form_validation->run() == FALSE) {

            $this->load->view('templates/header');
            $this->load->view('templates/menubar');
            $this->load->view('project/registration');
            $this->load->view('templates/footer');
        } else {

            $code = $this->input->post('CaptchaCode', true);
            $isHuman = $this->botdetectcaptcha->Validate($code);

            if (!$isHuman) {
                // TODO: Captcha validation failed, show error message
                $this->session->set_userdata('flash_message_type', 'error');
                $this->session->set_userdata('flash_message', 'Capcha Word Mismatch');

                $this->load->view('templates/header');
                $this->load->view('templates/menubar');
                $this->load->view('project_show/registration');
                $this->load->view('templates/footer');
            } else {
                // TODO: Captcha validation passed, perform protected action
                $data['project_name'] = $this->input->post('project_name', true);
                $data['project_type'] = $this->input->post('project_type', true);

                if ($this->project_show_model->is_registered($data['project_name'])) {
                    $this->session->set_userdata('flash_message_type', 'error');
                    $this->session->set_userdata('flash_message', 'Project Already Registered');

                    $this->load->view('templates/header');
                    $this->load->view('templates/menubar');
                    $this->load->view('project/registration');
                    $this->load->view('templates/footer');
                    return;
                }

                $config['upload_path'] = './assets/uploads/project_show/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '50';
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;

                $this->load->library('upload', $config);

                for ($I = 1; $I <= 5; $I++) {
                    if ($this->input->post('member' . '_' . $I . '_' . 'email')) {
                        if (!$this->upload->do_upload('member' . '_' . $I . '_' . 'photo')) {
                            $this->session->set_userdata('flash_message_type', 'error');
                            $this->session->set_userdata('flash_message', $this->upload->display_errors());
                            redirect('project_show/registration');
                        } else {
                            $file[$I - 1] = $this->upload->data();
                            $file_names[$I - 1] = 'assets/uploads/project_show/' . $file[$I - 1]['file_name'];
                        }
                    } else {

                        $file_names[$I - 1] = '';
                    }
                }
                $data['team_members'] = array(
                    array(
                        'student_id' => $this->input->post('member_1_student_id', true),
                        'institution' => $this->input->post('member_1_institution', true),
                        'department' => $this->input->post('member_1_department', true),
                        'name' => $this->input->post('member_1_name', true),
                        'email' => $this->input->post('member_1_email', true),
                        'phone' => $this->input->post('member_1_phone', true),
                        'address' => $this->input->post('member_1_address', true),
                        'photo' => $file_names[0],
                    ),
                    array(
                        'student_id' => $this->input->post('member_2_student_id', true),
                        'institution' => $this->input->post('member_2_institution', true),
                        'department' => $this->input->post('member_2_department', true),
                        'name' => $this->input->post('member_2_name', true),
                        'email' => $this->input->post('member_2_email', true),
                        'phone' => $this->input->post('member_2_phone', true),
                        'address' => $this->input->post('member_2_address', true),
                        'photo' => $file_names[1],
                    ),
                    array(
                        'student_id' => $this->input->post('member_3_student_id', true),
                        'institution' => $this->input->post('member_3_institution', true),
                        'department' => $this->input->post('member_3_department', true),
                        'name' => $this->input->post('member_3_name', true),
                        'email' => $this->input->post('member_3_email', true),
                        'phone' => $this->input->post('member_3_phone', true),
                        'address' => $this->input->post('member_3_address', true),
                        'photo' => $file_names[2],
                    ),
                    array(
                        'student_id' => $this->input->post('member_4_student_id', true),
                        'institution' => $this->input->post('member_4_institution', true),
                        'department' => $this->input->post('member_4_department', true),
                        'name' => $this->input->post('member_4_name', true),
                        'email' => $this->input->post('member_4_email', true),
                        'phone' => $this->input->post('member_4_phone', true),
                        'address' => $this->input->post('member_4_address', true),
                        'photo' => $file_names[3],
                    ),
                    array(
                        'student_id' => $this->input->post('member_5_student_id', true),
                        'institution' => $this->input->post('member_5_institution', true),
                        'department' => $this->input->post('member_5_department', true),
                        'name' => $this->input->post('member_5_name', true),
                        'email' => $this->input->post('member_5_email', true),
                        'phone' => $this->input->post('member_5_phone', true),
                        'address' => $this->input->post('member_5_address', true),
                        'photo' => $file_names[4],
                    ),
                );
                if ($this->project_show_model->create_team($data)) {

                    $team_info = $this->project_show_model->get_project_by_name($data['project_name']);
                    $message = "you have been registered seccessfully For BUET CSe Fest-2013 Project Show <br/> under Project Id: " .
                            $team_info['project_id'] .
                            "<br/>" .
                            "Project Name: " . $team_info['project_name'] .
                            "<br/>" .
                            "At Time : " . $team_info['registration_date_time']
                    ;

                    $this->session->set_userdata('receivers', $data['team_members']);
                    $this->session->set_userdata('subject', "Registration Information");
                    $this->session->set_userdata('message', $message);

                    $this->smtp_message_interface();

                    $this->session->set_userdata('flash_message_type', 'success');
                    $this->session->set_userdata('flash_message', 'Project Successfully Created');

                    redirect('project_show/registration');
                } else {
                    $this->session->set_userdata('flash_message_type', 'error');
                    $this->session->set_userdata('flash_message', 'Server problem Occured . Please try Again');

                    redirect('project_show/registration');
                }
            }
        }
    }

    public function set_rules() {

        $this->form_validation->set_rules('project_name', 'Team Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('project_type', 'Project Type', 'required');

        $this->form_validation->set_rules('CaptchaCode', 'Captcha', 'trim|required|xss_clean');

        $this->form_validation->set_rules('member_1_email', 'Member1 Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('member_1_phone', 'Member1 Phone', 'trim|required|xss_clean');
        $this->form_validation->set_rules('member_1_name', 'Member1 Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('member_1_department', 'Member1 Department', 'trim|required|xss_clean');
        $this->form_validation->set_rules('member_1_institution', 'Institution', 'trim|required|xss_clean');
    }

    public function init_capcha() {

        $captchaConfig = array(
            'CaptchaId' => 'ProjectCaptcha',
            'UserInputId' => 'CaptchaCode'
        );
        $this->load->library('BotDetect/BotDetectCaptcha', $captchaConfig);
    }

    public function smtp_message_interface() {

        $this->config->load('email', FALSE, TRUE);
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com';
        $config['smtp_port'] = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = $this->config->item('smtp_user');
        $config['smtp_pass'] = $this->config->item('smtp_pass');
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
        $config['validation'] = TRUE;

        $this->load->library('email', $config);
        foreach ($this->session->userdata('receivers') as $receiver) {

            $this->email->from($this->config->item('from'));
            $this->email->to($receiver['email']);
            $this->email->subject($this->session->userdata('subject'));
            $this->email->message($this->session->userdata('message'));

            if (!$this->email->send()) {
                //echo $this->email->print_debugger();
            }
        }

        $this->session->unset_userdata('subject');
        $this->session->unset_userdata('message');
        $this->session->unset_userdata('receivers');
    }

    public function registered_projects() {

        $data = array();
        $data['record'] = $this->project_show_model->get_valid_registered_teams();

        $this->load->view('templates/header');
        $this->load->view('templates/menubar');
        $this->load->view('project/registeredteams', $data);
        $this->load->view('templates/footer');
    }

    public function project_info($project_id) {

        $data['record'] = $this->project_show_model->get_project_info($project_id);
        $data['team_members'] = $this->project_show_model->get_project_members($project_id);

        $this->load->view('project/team_info', $data);
    }

}

