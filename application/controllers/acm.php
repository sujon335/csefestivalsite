<?php

class Acm extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('bsadd_model');
    }

    public function registration() {

        $this->set_rules();
        $this->init_capcha();

        if ($this->form_validation->run() == FALSE) {

            $this->load->view('templates/header');
            $this->load->view('templates/menubar');
            $this->load->view('acm/registration');
            $this->load->view('templates/footer');
        } else {

            $code = $this->input->post('CaptchaCode', true);
            $isHuman = $this->botdetectcaptcha->Validate($code);

            if (!$isHuman) {
                // TODO: Captcha validation failed, show error message
                $this->session->set_userdata('flash_message_type', 'error');
                $this->session->set_userdata('flash_message', 'Capcha Word Mismatch');

                $this->load->view('templates/header');
                $this->load->view('templates/menubar');
                $this->load->view('acm/registration');
                $this->load->view('templates/footer');
            } else {
                // TODO: Captcha validation passed, perform protected action

                $data['field'] = $this->input->post('field', true);
                $data['institution'] = $this->input->post('institution', true);

                $data['team_name'] = $this->input->post('team_name', true);

                if ($this->bsadd_model->is_registered($data['field'], $data['team_name'])) {
                    $this->session->set_userdata('flash_message_type', 'error');
                    $this->session->set_userdata('flash_message', 'Team  Name Already Registered');

                    $this->load->view('templates/header');
                    $this->load->view('templates/menubar');
                    $this->load->view('acm/registration');
                    $this->load->view('templates/footer');

                    return;
                }

                $data['team_members'] = array(
                    array(
                        'student_id' => $this->input->post('member_1_student_id', true),
                        'department' => $this->input->post('member_1_department', true),
                        'name' => $this->input->post('member_1_name', true),
                        'email' => $this->input->post('member_1_email', true),
                        'phone' => $this->input->post('member_1_phone', true),
                        'address' => $this->input->post('member_1_address', true),
                        'photo' => $this->input->post('member_1_photo', true),
                    ),
                    array(
                        'department' => $this->input->post('member_2_department', true),
                        'student_id' => $this->input->post('member_2_student_id', true),
                        'name' => $this->input->post('member_2_name', true),
                        'email' => $this->input->post('member_2_email', true),
                        'phone' => $this->input->post('member_2_phone', true),
                        'address' => $this->input->post('member_2_address', true),
                        'photo' => $this->input->post('member_2_photo', true),
                    ),
                    array(
                        'department' => $this->input->post('member_3_department', true),
                        'student_id' => $this->input->post('member_3_student_id', true),
                        'name' => $this->input->post('member_3_name', true),
                        'email' => $this->input->post('member_3_email', true),
                        'phone' => $this->input->post('member_3_phone', true),
                        'address' => $this->input->post('member_3_address', true),
                        'photo' => $this->input->post('member_3_photo', true),
                    ),
                );

                if ($this->bsadd_model->create_team($data)) {

                    $team_info = $this->bsadd_model->get_team_info($data['field'], $data['team_name']);
                    $subject = "Registration Information";
                    $message = "you have been registered seccessfully " . "For Intra-CSE Programming Contest<br/>" . "under Team Id: " .
                            $team_info['team_id'] .
                            "<br/>" .
                            "Team Name: " . $team_info['team_name'] .
                            "<br/>" .
                            "At Time : " . $team_info['registration_date_time'] .
                            '<br/>'
                    ;

                    $this->session->set_userdata('subject', $subject);
                    $this->session->set_userdata('message', $message);
                    $this->session->set_userdata('receivers', $data['team_members']);

                    $this->smtp_message_interface();

                    $this->session->set_userdata('flash_message_type', 'success');
                    $this->session->set_userdata('flash_message', 'Team Successfully Created.Please Check your email Fro Further Notification');

                    redirect('bsadd/registration');
                } else {
                    $this->session->set_userdata('flash_message_type', 'error');
                    $this->session->set_userdata('flash_message', 'Incomplete Registration<br/><h1>Please try Again Later</h1>');

                    redirect('acm/registration');                   
                }
            }
        }
    }

    public function set_rules() {

        $this->form_validation->set_rules('team_name', 'Team Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('CaptchaCode', 'Captcha', 'trim|required|xss_clean');

        $this->form_validation->set_rules('member_1_email', 'Member1 Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('member_2_email', 'Member2 Email', 'trim|required|valid_email');

        $this->form_validation->set_rules('member_1_phone', 'Member1 Phone', 'trim|required|xss_clean');
        $this->form_validation->set_rules('member_2_phone', 'Member2 Phone', 'trim|required|xss_clean');

        $this->form_validation->set_rules('member_1_name', 'Member1 Name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('member_2_name', 'Member2 Name', 'trim|required|xss_clean');

        $this->form_validation->set_rules('member_1_department', 'Member1 Department', 'trim|required|xss_clean');
        $this->form_validation->set_rules('member_2_department', 'Member2 Department', 'trim|required|xss_clean');
    }

    public function init_capcha() {

        $captchaConfig = array(
            'CaptchaId' => 'AcmCaptcha',
            'UserInputId' => 'CaptchaCode'
        );
        $this->load->library('BotDetect/BotDetectCaptcha', $captchaConfig);
    }

    public function smtp_message_interface() {

        $this->config->load('email', FALSE, TRUE);
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com';
        $config['smtp_port'] = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = $this->config->item('smtp_user');
        $config['smtp_pass'] = $this->config->item('smtp_pass');
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
        $config['validation'] = TRUE;

        $this->load->library('email', $config);
        foreach ($this->session->userdata('receivers') as $receiver) {

            $this->email->from($this->config->item('from'));
            $this->email->to($receiver['email']);
            $this->email->subject($this->session->userdata('subject'));
            $this->email->message($this->session->userdata('message'));

            if (!$this->email->send()) {
                //echo $this->email->print_debugger();
            }
        }

        $this->session->unset_userdata('subject');
        $this->session->unset_userdata('message');
        $this->session->unset_userdata('receivers');
    }

    public function team_info($team_id) {
        $data = array();
        $data['record'] = $this->acm_model->get_team_info($team_id);
        $data['team_members'] = $this->acm_model->get_team_members($team_id);

        $this->load->view('acm/team_info', $data);
    }

    public function contest_registered_teams() {

        $data = array();
        $data['record'] = $this->acm_model->get_registered_teams();

        $data['field'] = 'Intra-BUET Programming Contest';

        $this->load->view('templates/header');
        $this->load->view('templates/menubar');
        $this->load->view('acm/registeredteams', $data);
        $this->load->view('templates/footer');
    }

    public function ranklist() {

        $this->load->view('templates/header');
        $this->load->view('');
    }

}