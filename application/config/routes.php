<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 | -------------------------------------------------------------------------
 | URI ROUTING
 | -------------------------------------------------------------------------
 | This file lets you re-map URI requests to specific controller functions.
 |
 | Typically there is a one-to-one relationship between a URL string
 | and its corresponding controller class/method. The segments in a
 | URL normally follow this pattern:
 |
 * 	example.com/class/method/id/
 |
 | In some instances, however, you may want to remap this relationship
 | so that a different class/function is called than the one
 | corresponding to the URL.
 |
 | Please see the user guide for complete details:
 |
 * 	http://codeigniter.com/user_guide/general/routing.html
 |
 | -------------------------------------------------------------------------
 | RESERVED ROUTES
 | -------------------------------------------------------------------------
 |
 | There area two reserved routes:
 |
 * 	$route['default_controller'] = 'welcome';
 |
 | This route indicates which controller class should be loaded if the
 | URI contains no data. In the above example, the "welcome" class
 | would be loaded.
 |
 * 	$route['404_override'] = 'errors/page_missing';
 |
 | This route will tell the Router what URI segments to use if those provided
 | in the URL cannot be matched to a valid route.
 |
 */

$route['contest/mockup/register'] = 'mockup/registration';
$route['contest/mockup/registration'] = 'mockup/registration';
$route['contest/logic-olympiad/register'] = 'logic_olympiad/registration';
$route['contest/logic-olympiad/registration'] = 'logic_olympiad/registration';
$route['contest/unknown-language/registration'] = 'unknown_language/registration';
$route['contest/unknown-language/register'] = 'unknown_language/registration';
$route['contest/acm/registration'] = 'acm/registration';
$route['contest/acm/register'] = 'acm/registration';
$route['contest/robotics/registration'] = 'robotics/registration';
$route['contest/robotics/register'] = 'robotics/registration';
$route['contest/ai-challenge/register'] = 'ai/registration';
$route['contest/ai-challenge/registration'] = 'ai/registration';
$route['contest/project-show/register'] = "project_show/registration";
$route['contest/project-show/registration'] = "project_show/registration";
$route['contest/dev-mercenaries/register'] = "bsadd/registration";
$route['contest/dev-mercenaries/registration'] = "bsadd/registration";
$route['contest/(:any)/details'] = "page/map/$1";
$route['welcome'] = 'home';
$route['welcome/index'] = 'home';
$route['default_controller'] = "home";
$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */