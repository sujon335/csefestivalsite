<?php

class Admin_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insert_image($data) {

        $data['photo_id'] = NULL;

        if ($this->db->insert('gallery', $data)) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_image($photo_id) {

        $this->db->where('photo_id', $photo_id)->delete('gallery');
    }

    public function get_images() {

        return $this->db->order_by('photo_id', 'desc')->get('gallery')->result_array();
    }

    public function get_team($field, $team_name) {
        if ($field != 'project_show') {
            $contest_id = $this->db->select('contest_id')->where('field', $field)->get('bsadd_contests')->row_array();
            $contest_id = $contest_id['contest_id'];

            return $this->db->where('contest_id', $contest_id)
                    ->where('team_name', $team_name)
                    ->get('bsadd_contest_teams')
                    ->row_array();
        } else {

            return $this->db->where('project_name', $team_name)
                    ->get('projects')
                    ->row_array();
        }
    }

}
