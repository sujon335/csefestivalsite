<br/>
<br/>
<center><h1 class="custom-font">Intra-CSE Programming Contest Registration</h1></center>
<br/>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span2">
            <div class="row-fluid">
                <div class="span12">
                    <?php if ($this->session->userdata('flash_message_type')): ?>
                        <div class="alert alert-<?php echo $this->session->userdata('flash_message_type'); ?> larger">
                            <?php echo $this->session->userdata('flash_message'); ?>
                        </div>
                        <?php $this->session->unset_userdata('flash_message_type'); ?>
                        <?php $this->session->unset_userdata('flash_message'); ?>
                    <?php endif; ?>
                </div>
                <?php if (validation_errors()): ?>
                    <div class="span12">
                        <div class="alert alert-error larger">
                            <?php echo validation_errors(); ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="span8">
            <?php echo form_open('acm/registration'); ?>
            <div class="row-fluid">
                <div class="span4">
                    <fieldset>
                        <!--<label for="field">Category</label>-->
                        <input type="hidden" name="field" value="acm"/>

                        <label for="team_name">Team Name</label>
                        <input name="team_name" type="text" value="<?php echo set_value('team_name'); ?>"/>
                        <!--<label for="institution">Institution</label>-->
                        <input name="institution" type="hidden" value="BUET"/>
                        <br/>
                        <br/>
                        <label for="CapchaCode">Please type the word in the picture</label>
                        <link type="text/css" rel="Stylesheet" href="<?php echo CaptchaUrls::LayoutStylesheetUrl(); ?>" />
                        <?php echo $this->botdetectcaptcha->Html(); ?>
                        <input type="text" name="CaptchaCode" id="CaptchaCode" value="" />
                    </fieldset>
                </div>
                <div class="span8">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#member1" data-toggle="tab">First member</a></li>
                        <li><a href="#member2" data-toggle="tab">Second Member</a></li>
                        <li><a href="#member3" data-toggle="tab">Third Member</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="member1" class="tab-pane active">
                            <fieldset>
                                <label for="member_1_name">First Member Info:</label>
                                <label>Student Id</label>
                                <input name="member_1_student_id" type="text" value="<?php echo set_value('member_1_student_id'); ?>"/>
                                <label for="member_1_department">Department</label>
                                <input name="member_1_department" type="text" value="<?php echo set_value('member_1_department'); ?>"/>
                                <label for="member_1_name">Name</label>
                                <input name="member_1_name" type="text" value="<?php echo set_value('member_1_name'); ?>"/>
                                <label for="member_1_email">Email</label>
                                <input name="member_1_email" type="text" value="<?php echo set_value('member_1_email'); ?>"/>
                                <label for="member_1_phone">Phone</label>
                                <input name="member_1_phone" type="text" value="<?php echo set_value('member_1_phone'); ?>"/>
                                <label for="member_1_address">Address</label>
                                <textarea name="member_1_address" row="4" col="5"></textarea>
                                <!--<label for="member_1_photo">Photo</label>-->
                                <input name="member_1_photo" type="hidden" value=""/>
                                <!--<p class="help-text" style="color: red">Size must be within 50KB and max resolution 300 * 300 (png | gif | jpg)</p>-->
                            </fieldset>
                        </div>
                        <div id="member2" class="tab-pane">
                            <fieldset>
                                <label for="member_2_name">Second Member Info:</label>
                                <label>Student Id</label>
                                <input name="member_2_student_id" type="text" value="<?php echo set_value('member_2_student_id'); ?>"/>
                               <label for="member_1_department">Department</label>
                                <input name="member_2_department" type="text" value="<?php echo set_value('member_2_department'); ?>"/>
                                <label for="member_2_name">Name</label>
                                <input name="member_2_name" type="text" value="<?php echo set_value('member_2_name'); ?>"/>
                                <label for="member_2_email">Email</label>
                                <input name="member_2_email" type="text" value="<?php echo set_value('member_2_email'); ?>"/>
                                <label for="member_2_phone">Phone</label>
                                <input name="member_2_phone" type="text" value="<?php echo set_value('member_2_phone'); ?>"/>
                                <label for="member_2_address">Address</label>
                                <textarea name="member_2_address" row="4" col="5"></textarea>
                                <!--<label for="member_1_photo">Photo</label>-->
                                <input name="member_2_photo" type="hidden" value=""/>
                                <!--<p class="help-text" style="color: red">Size must be within 50KB and max resolution 300 * 300 (png | gif | jpg)</p>-->
                            </fieldset>
                        </div>
                        <div id="member3" class="tab-pane">
                            <fieldset>
                                <label for="member_3_name">Third Member Info:</label>
                                <label>Student Id</label>
                                <input name="member_3_student_id" type="text" value="<?php echo set_value('member_3_student_id'); ?>"/>
                                <label for="member_3_department">Department</label>
                                <input name="member_3_department" type="text" value="<?php echo set_value('member_3_department'); ?>"/>
                                <label for="member_3_name">Name</label>
                                <input name="member_3_name" type="text" value="<?php echo set_value('member_3_name'); ?>"/>
                                <label for="member_3_email">Email</label>
                                <input name="member_3_email" type="text" value="<?php echo set_value('member_3_email'); ?>"/>
                                <label for="member_3_phone">Phone</label>
                                <input name="member_3_phone" type="text" value="<?php echo set_value('member_3_phone'); ?>"/>
                                <label for="member_3_address">Address</label>
                                <textarea name="member_3_address" row="4" col="5"></textarea>
                                <!--<label for="member_3_photo">Photo</label>-->
                                <input name="member_3_photo" type="hidden" value=""/>
                                <!--<p class="help-text" style="color: red">Size must be within 50KB and max resolution 300 * 300 (png | gif | jpg)</p>-->
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
            <fieldset>
                <input class="btn btn-large btn-primary" type="submit" value="SUBMIT"/>
                <a class="btn btn-large" href="<?php echo base_url() ?>index.php/home/index">Cancel</a>
            </fieldset>
            <?php echo form_close(); ?>
        </div>  
    </div>
</div>