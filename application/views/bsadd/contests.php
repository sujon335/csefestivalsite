<br/>
<br/>
<br/>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span4"></div>
        <div class="span6">
            <table class="table table-striped">
                <thead>Contest List</thead>
                <tbody>
                    <?php foreach ($record as $row): ?>
                                        <!-- <a></a> -->

                                        <tr>
                                            <td><?php echo $row['field']; ?></td>
                                            <td><?php echo $row['last_date_time']; ?></td>
                        <?php if ($row['field']!='project_show'): ?>
                                                <td><a class="btn btn-primary larger" target="blank" href="<?php echo base_url(); ?>index.php/bsadd/contest_registered_teams/<?php echo $row['field']; ?>">View Teams</a></td>
                        <?php else: ?>
                                                <td><a class="btn btn-primary larger" target="blank" href="<?php echo base_url();?>index.php/project_show/registered_projects">View Projects</a></td>
                        <?php endif; ?>
                                                <td><a class="btn btn-success larger" href="<?php echo base_url(); ?>index.php/admin/search_team/<?php echo $row['field']; ?>">Search Team</a></td>
                                                <td><a class="btn btn-danger larger" href="<?php echo base_url(); ?>index.php/admin/delete_team/<?php echo $row['field']; ?>">Delete Team</a></td>
                                            </tr>


                    <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
