<div class="container-fluid">
    <div class="row-fluid">
        <div class="span2"></div>
        <div class="span7">
            <table class="table table-striped">
                <tr>
                    <td>Team</td>
                    <td><?php echo $record['team_name']; ?></td>
                </tr>
                <tr>
                    <td> Institution</td>
                    <td><?php echo $record['institution']; ?></td>
                </tr>
            </table>
            <br/>
            <ul class="nav nav-tabs">
                <?php for ($I = 0; $I < count($team_members); $I++) { ?>
                    <li><a href="#member<?php echo $I + 1; ?>" data-toggle="tab">Member<?php echo $I + 1; ?></a></li>
                <?php } ?>
            </ul>
            <div class="tab-content">
                <?php for ($I = 0; $I < count($team_members); $I++) { ?>
                    <div id="member<?php echo ($I + 1); ?>" class="tab-pane">
                        <div class="container-fluid">
                            <div class="row-fluid">
                                <?php if ($this->session->userdata('admin_email')): ?>
                                    <div class="span12">
                                        <img src="<?php echo base_url() . $team_members[$I]['photo'] ?>"/>
                                    </div>
                                <?php endif; ?>
                                <div class="span12">
                                    <table class="table table-striped">
                                        <tr>
                                            <td>Department</td>
                                            <td><?php echo $team_members[$I]['department']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Name</td>
                                            <td><?php echo $team_members[$I]['name'] ?></td>
                                        </tr>
                                        <?php if ($this->session->userdata('admin_email')): ?>
                                            <tr>
                                                <td>Student Id</td>
                                                <td><?php echo $team_members[$I]['student_id']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td><?php echo $team_members[$I]['email']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Phone</td>
                                                <td><?php echo $team_members[$I]['phone']; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Address</td>
                                                <td><?php echo $team_members[$I]['address']; ?></td>
                                            </tr>
                                        <?php endif; ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>