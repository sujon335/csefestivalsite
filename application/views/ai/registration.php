<br/>
<br/>
<center><h1 class="custom-font">AI-Challenge Registration</h1></center>
<br/>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span2">
            <div class="row-fluid">
                <div class="span12">
                    <?php if ($this->session->userdata('flash_message_type')): ?>
                        <div class="alert alert-<?php echo $this->session->userdata('flash_message_type'); ?> larger">
                            <?php echo $this->session->userdata('flash_message'); ?>
                        </div>
                        <?php $this->session->unset_userdata('flash_message_type'); ?>
                        <?php $this->session->unset_userdata('flash_message'); ?>
                    <?php endif; ?>
                </div>
                <?php if (validation_errors()): ?>
                    <div class="span12">
                        <div class="alert alert-error larger">
                            <?php echo validation_errors(); ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="span8">
            <?php echo form_open('ai/registration', array('enctype' => 'multipart/form-data')); ?>
            <div class="row-fluid">
                <div class="span4">
                    <fieldset>
                        <!--<label for="field">Category</label>-->
                        <input type="hidden" name="field" value="ai"/>
                        <!--<label for="team_name">Team Name</label>-->
                        <input id="team_name" name="team_name" type="hidden"/>
                        <br/>
                        <br/>
                        <label for="CapchaCode">Please type the word in the picture</label>
                        <link type="text/css" rel="Stylesheet" href="<?php echo CaptchaUrls::LayoutStylesheetUrl(); ?>" />
                        <?php echo $this->botdetectcaptcha->Html(); ?>
                        <input type="text" name="CaptchaCode" id="CaptchaCode" value="" />
                    </fieldset>
                </div>
                <div class="span8">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#member1" data-toggle="tab">Personal Information</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="member1" class="tab-pane active">
                            <fieldset>
                                <label>Student Id</label>
                                <input name="member_1_student_id" type="text" value="<?php echo set_value('member_1_student_id'); ?>"/>
                                <label for="institution">Institution</label>
                                <input name="institution" type="text" value="<?php echo set_value('institution'); ?>"/>
                                <label for="member_1_department">Department</label>
                                <input name="member_1_department" type="text" value="<?php echo set_value('member_1_department'); ?>"/>
                                <label for="member_1_name">Name</label>
                                <input name="member_1_name" type="text" value="<?php echo set_value('member_1_name'); ?>"/>
                                <label for="member_1_email">Email</label>
                                <input id="member_1_email" name="member_1_email" type="text" value="<?php echo set_value('member_1_email'); ?>"/>
                                <label for="member_1_phone">Phone</label>
                                <input name="member_1_phone" type="text" value="<?php echo set_value('member_1_phone'); ?>"/>
                                <label for="member_1_address">Address</label>
                                <textarea name="member_1_address" row="4" col="5"></textarea>
                                <label for="member_1_photo">Photo</label>
                                <input name="member_1_photo" type="file" value="<?php echo set_value('member_1_photo'); ?>"/>
                                <p class="help-text" style="color: red">Size must be within 50KB</p>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
            <fieldset>
                <input class="btn btn-large btn-primary" type="submit" value="SUBMIT"/>
                <a class="btn btn-large" href="<?php echo base_url() ?>index.php/home/index">Cancel</a>
            </fieldset>
            <?php echo form_close(); ?>

            <script type="text/javascript">

                $(document).ready(function(){
                    email = $('#member_1_email').attr('value');
                    $('#team_name').attr('value',email);
                });

                $('#member_1_email').change(function(){
                    email = $('#member_1_email').attr('value');
                    $('#team_name').attr('value',email);
                });
            </script>
        </div>
    </div>
</div>