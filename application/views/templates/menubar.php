<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <a class="brand" href="<?php echo base_url(); ?>index.php">CSE Festival-2013</a>
        <ul class="nav">
            <li><a href="<?php echo base_url(); ?>index.php">Home</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/page/view?page=schedule">Schedule</a></li>
            <li class="dropdown">
                <a id="dropdown_competitions" class="dropdown-toggle" data-toggle="dropdown" roll="button" href="#">Competitions <b class="caret"></b></a>
                <ul class="dropdown-menu" area-labelledby="dropdown_competitions" role="menu">
                    <li class="dropdown-submenu">
                        <a href="#" role="button">Inter-University Competitions</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-submenu">
                                <a href="<?php echo base_url(); ?>contest/dev-mercenaries/details" role="button">Dev-Mercenaries Prototyping Challenge</a>
                                <ul class="dropdown-menu">
                                    <li role="presentation"><a href="<?php echo base_url(); ?>contest/dev-mercenaries/details">Details</a></li>
                                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/page/view?page=system_prototyping_contest_rules" role="menuitem">Rules and Regulation</a></li>
                                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/page/view?page=system_prototyping_contest_schedule" role="menuitem">Schedule</a></li>
                                    <li role="presentation"><a href="<?php echo base_url(); ?>contest/dev-mercenaries/register" role="menuitem">Register</a></li>
                                    <li class="dropdown-submenu" role="presentation">
                                        <a href="#" role="menuitem">PHP</a>
                                        <ul class="dropdown-menu">
                                            <li role="presentation"><a href="<?php echo base_url(); ?>index.php/bsadd/contest_registered_teams/PHP">Registered Teams</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu" role="presentation">
                                        <a href="#" role="menuitem">Android</a>
                                        <ul class="dropdown-menu">
                                            <li role="presentation"><a href="<?php echo base_url(); ?>index.php/bsadd/contest_registered_teams/Android">Registered Teams</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu" role="presentation">
                                        <a href="#" role="menuitem">Windows Phone & Windows 8</a>
                                        <ul class="dropdown-menu">
                                            <li role="presentation"><a href="<?php echo base_url(); ?>index.php/bsadd/contest_registered_teams/Windows">Registered Teams</a></li>
                                        </ul>
                                    </li>
                                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/page/view?page=system_prototyping_contest_ranklist">Ranklist</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu">
                                <a href="<?php echo base_url(); ?>index.php/page/view?page=project_show" role="menuitem">project Show</a>
                                <ul class="dropdown-menu">
                                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/page/view?page=project_show">Details</a></li>
                                    <li role="presentation">
                                        <a href="<?php echo base_url(); ?>contest/project-show/register" role="menuitem">Register</a>                                        
                                    </li>
                                    <li role="presentation"><a href="<?php echo base_url();?>index.php/project_show/registered_projects">Registered Projects</a></li>
                                    <li role="presentation"><a  href="<?php echo base_url();?>index.php/page/view?page=project_show_result" role="menuitem">Result</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu">
                                <a href="<?php echo base_url(); ?>index.php/page/view?page=ai_contest" role="menuitem">AI Challenge</a>
                                <ul class="dropdown-menu">
                                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/page/view?page=ai_contest">Details</a></li>
                                    <li role="presentation">
                                        <a href="<?php echo base_url(); ?>contest/ai-challenge/register" role="menuitem">Register</a>
                                    </li>
                                    <li role="presentation">
                                        <a href="<?php echo base_url(); ?>index.php/page/view?page=ai_submission">Submit Bot</a>
                                    </li>
                                    <li role="presentation">
                                        <a href="<?php echo base_url(); ?>index.php/bsadd/contest_registered_teams/ai">Contestants</a>
                                    </li>
                                    <li role="presentation">
                                        <a href="<?php echo base_url(); ?>index.php/page/view?page=ai_contest_ranklist">Ranklist</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown-submenu">
                        <a href="#" role="menuitem">Intra-CSE Competitions</a>
                        <ul class="dropdown-menu">
                            <li  class="dropdown-submenu" role="presentation">
                                <a href="#" role="menuitem">Programming Contest</a>
                                <ul class="dropdown-menu">
                                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/page/view?page=programming_contest">Details</a></li>
                                    <li role="presentation"><a href="<?php echo base_url(); ?>contest/acm/registration" role="menuitem">Register</a></li>
                                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/bsadd/contest_registered_teams/acm" role="menuitem">Registered Teams</a></li>
                                    <!-- href="<?php echo base_url(); ?>index.php/acm/ranklist" -->
                                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/page/view?page=programming_contest_ranklist" role="menuitem">Ranklist</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu" role="menuitem">
                                <a href="<?php echo base_url(); ?>index.php/page/view?page=robotics_competition">Robotics Competition</a>
                                <ul class="dropdown-menu">
                                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/page/view?page=robotics_competition">Details</a></li>
                                    <li role="presentation"><a href="<?php echo base_url(); ?>contest/robotics/registration" role="menuitem">Register</a></li>
                                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/bsadd/contest_registered_teams/robotics" role="menuitem">Registered Teams</a></li>
                                    <!-- href="<?php echo base_url(); ?>index.php/acm/ranklist" -->
                                    <li role="presentation"><a  href="<?php echo base_url(); ?>index.php/page/view?page=robotics_contest_ranklist" role="menuitem">Ranklist</a></li>
                                </ul>
                            </li>
                                                        <li class="dropdown-submenu" role="presentation">
                                <a href="<?php echo base_url(); ?>index.php/page/view?page=intra_system_analysis_contest" role="menuitem">Software Design & Mock UI Contest</a>
                                <ul class="dropdown-menu">
                                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/page/view?page=intra_system_analysis_contest">Details</a></li>
                                    <li role="presentation"><a href="<?php echo base_url(); ?>contest/mockup/registration" role="menuitem">Register</a></li>
                                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/bsadd/contest_registered_teams/mockup" role="menuitem">Registered Teams</a></li>
                                    
                                    <li role="presentation"><a  href="<?php echo base_url(); ?>index.php/page/view?page=mockup_ranklist" role="menuitem">Ranklist</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu" role="presentation">
                                <a href="<?php echo base_url(); ?>index.php/page/view?page=unknown_language_contest" role="menuitem">Unknown Language Contest</a>
                                <ul class="dropdown-menu">
                                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/page/view?page=unknown_language_contest">Details</a></li>
                                    <li role="presentation"><a href="<?php echo base_url(); ?>contest/unknown-language/registration" role="menuitem">Register</a></li>
                                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/bsadd/contest_registered_teams/unknown_language" role="menuitem">Registered Teams</a></li>
                                    <!-- href="<?php echo base_url(); ?>index.php/acm/ranklist" -->
                                    <li role="presentation"><a  href="<?php echo base_url(); ?>index.php/page/view?page=unknown_language_contest_ranklist" role="menuitem">Ranklist</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu"role="presentation">
                                <a href="<?php echo base_url(); ?>index.php/page/view?page=puzzle_contest" role="menuitem">Puzzle Contest</a>
                                <ul class="dropdown-menu">                                    
                                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/page/view?page=puzzle_contest_ranklist" role="menuitem">Ranklist</a></li>
                                </ul>
                            </li>
                              <li class="dropdown-submenu" role="presentation">
                                <a href="<?php echo base_url(); ?>index.php/page/view?page=logic_olympiad" role="menuitem">Logic Olympiad</a>
                                <ul class="dropdown-menu">
                                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/page/view?page=logic_olympiad">Details</a></li>
                                    <li role="presentation"><a href="<?php echo base_url(); ?>contest/logic-olympiad/register">Register</a></li>
                                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/bsadd/contest_registered_teams/logic_olympiad">Registered Teams</a></li>
                                    <li role="presentation"><a  href="<?php echo base_url(); ?>index.php/page/view?page=logic_olympiad_ranklist" role="menuitem">Ranklist</a></li>
                                </ul>
                            </li>
                            <li role="presentation"><a href="<?php echo base_url();?>puzzle/index.php" role="menuitem">Picture Puzzle</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a  id="dropdown_event" class="dropdown-toggle" data-toggle="dropdown" href="#">Events <b class="caret"></b></a>
                <ul class="dropdown-menu" area-labelledby="dropdown_event" role="menu">
                    <li class="" role="presentation"><a href="<?php echo base_url(); ?>index.php/page/view?page=seminar" role="menuitem">Seminar</a></li>
                    <li class="" role="presentation"><a href="#" role="menuitem">Job Fair</a></li>
                    <li role="presentation"><a href="<?php echo base_url(); ?>index.php/page/view?page=cultural_night" role="menuitem">Cultural Night</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a id="dropdown_games" class="dropdown-toggle" data-toggle="dropdown"  href="<?php echo base_url(); ?>index.php/page/view?page=gamesNsports" roll="menuitem"> Games & Sports<b class="caret"></b></a>
                <ul class="dropdown-menu" area-labelledby="dropdown_games" roll="menu">
                    <li class="presentation" role="menuitem"><a href="<?php echo base_url(); ?>index.php/page/view?page=GamesNsports">Overview</a></li>
                </ul>
            </li>
            <li><a href="<?php echo base_url();?>index.php/page/view?page=alumni_registration">Alumni Registration</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/home/gallery">Gallery</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/page/view?page=media">Media Center</a></li>
   
            <li><a href="<?php echo base_url(); ?>index.php/page/view?page=contact_us">Contact Us</a></li>
        </ul>
    </div>
</div>
