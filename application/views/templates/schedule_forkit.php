<link rel="stylesheet" media="all" href="<?php echo base_url(); ?>assets/css/forkit_base.css"/>
<link rel="stylesheet" media="all" href="<?php echo base_url(); ?>assets/css/forkit.css"/>

<div class="forkit-curtain">
    <div class="close-button"></div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span2">
                <div class="row-fluid">
                    <div class="span12">
                        <!-- image Slide gallery -->
                        <div class="popover top custom-popup" style="display: block;">
                            <div class="arrow"></div>
                            <h3 class="popover-title">Glimpse</h3>
                            <div class="popover-content">
                                <marquee style="width: auto;" direction="up" scrollamount="2" onmouseover="this.stop();" onmouseout="this.start();">
                                    <?php $this->load->view('templates/gallery_widget'); ?>
                                </marquee>
                            </div>
                        </div>
                    </div>
                    <div class="span12 btn btn-danger" style="background-color: transparent;">
                        <!-- Notice board -->
                        <?php $this->load->view('templates/notice_board_widget');?>
                    </div>
                </div>
                <!-- News Feed -->

            </div>
            <div class="span1"></div>
          <div class="span8" style="color: white;">
              
              <?php $this->load->view('templates/schedule_widget');?>
               <!-- <a href="<?php echo base_url();?>index.php/page/view?page=schedule"><img src="<?php echo base_url();?>assets/images/schedule.jpg"/></a>-->
            </div>
        </div>
    </div>
</div>
<!-- The ribbon -->
<a class="forkit" data-text="Schedule" data-text-detached="Schedule" href=""><img style="position: absolute; top: 0; right: 0; border: 0;" src="https://a248.e.akamai.net/camo.github.com/e6bef7a091f5f3138b8cd40bc3e114258dd68ddf/687474703a2f2f73332e616d617a6f6e6177732e636f6d2f6769746875622f726962626f6e732f666f726b6d655f72696768745f7265645f6161303030302e706e67" alt="Fork me on GitHub"></a>

<script src="<?php echo base_url(); ?>assets/js/forkit.js"></script>
