<br/>
<br/>
<br/>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span2">
            <div class="row-fluid">
                <div class="span12">
                    <?php if ($this->session->userdata('flash_message_type')): ?>
                        <div class="alert alert-<?php echo $this->session->userdata('flash_message_type');?> larger">
                            <?php echo $this->session->userdata('flash_message'); ?>
                        </div>
                        <?php $this->session->unset_userdata('flash_message_type'); ?>
                        <?php $this->session->unset_userdata('flash_message'); ?>
                    <?php endif; ?>  
                </div>
                <?php if (validation_errors()): ?>
                    <div class="span12">
                        <div class="alert alert-error larger">
                            <?php echo validation_errors(); ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="span10">
            <?php echo form_open('project_show/registration', array('enctype' => 'multipart/form-data')); ?>
            <div class="row-fluid">
                <div class="span4">
                    <fieldset>
                        <label for="project_name">Project Name</label>
                        <input name="project_name" type="text"  value="<?php echo set_value('project_name'); ?>"/>
                        <label>Project Type</label>
                        <select name="project_type">
                            <option value="software">Software</option>
                            <option value="hardware">Hardware</option>
                        </select>
                        <br/>
                        <br/>
                        <label for="CapchaCode">Please type the word in the picture</label>
                        <link type="text/css" rel="Stylesheet" href="<?php echo CaptchaUrls::LayoutStylesheetUrl(); ?>" />
                        <?php echo $this->botdetectcaptcha->Html(); ?>
                        <input type="text" name="CaptchaCode" id="CaptchaCode" value="" />
                        <br/>
                        <br/>
                        <fieldset>
                            <input class="btn btn-large btn-primary" type="submit" value="SUBMIT"/>
                            <a class="btn btn-large" href="<?php echo base_url() ?>index.php/home/index">Cancel</a>
                        </fieldset>
                    </fieldset>
                </div>
                <div class="span8">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#member1" data-toggle="tab">First member</a></li>
                        <li><a href="#member2" data-toggle="tab">Second Member</a></li>
                        <li><a href="#member3" data-toggle="tab">Third Member</a></li>
                        <li><a href="#member4" data-toggle="tab">Fourth Member</a></li>
                        <li><a href="#member5" data-toggle="tab">Fifth Member</a></li>                        
                    </ul>
                    <div class="tab-content">
                        <div id="member1" class="tab-pane active">
                            <fieldset>
                                <label>Student Id</label>
                                <input name="member_1_student_id" type="text" value="<?php echo set_value('member_1_student_id'); ?>"/>
                                <label for="member_1_institution">Institution</label>
                                <input name="member_1_institution" type="text"  value="<?php echo set_value('member_1_institution'); ?>"/>
                                <label for="member_1_department">Department</label>
                                <input name="member_1_department" type="text" value="<?php echo set_value('member_1_department'); ?>"/>
                                <label for="member_1_name">Name</label>
                                <input name="member_1_name" type="text" value="<?php echo set_value('member_1_name'); ?>"/>
                                <label for="member_1_email">Email</label>
                                <input name="member_1_email" type="text"  value="<?php echo set_value('member_1_email'); ?>"/>
                                <label for="member_1_phone">Phone</label>
                                <input name="member_1_phone" type="text"  value="<?php echo set_value('member_1_phone'); ?>"/>
                                <label for="member_1_address">Address</label>
                                <textarea name="member_1_address" row="4" col="5"></textarea>
                                <label for="member_1_photo">Photo</label>
                                <input type="file" name="member_1_photo"/>
                                <p class="help-text" style="color: red">Size must be within 50KB</p>
                            </fieldset>
                        </div>
                        <div id="member2" class="tab-pane">
                            <fieldset>
                                <label>Student Id</label>
                                <input name="member_2_student_id" type="text" />
                                <label for="member_2_institution">Institution</label>
                                <input name="member_2_institution" type="text" />
                                <label for="member_2_department">Department</label>
                                <input name="member_2_department" type="text" />
                                <label for="member_2_name">Name</label>
                                <input name="member_2_name" type="text"  />
                                <label for="member_2_email">Email</label>
                                <input name="member_2_email" type="text"  />
                                <label for="member_2_phone">Phone</label>
                                <input name="member_2_phone" type="text"  />
                                <label for="member_2_address">Address</label>
                                <textarea name="member_2_address" row="4" col="5"></textarea>
                                <label for="member_2_photo">Photo</label>
                                <input type="file" name="member_2_photo"/>
                                <p class="help-text" style="color: red">Size must be within 50KB</p>
                            </fieldset>

                        </div>
                        <div id="member3" class="tab-pane">
                            <fieldset>
                                <label>Student Id</label>
                                <input name="member_3_student_id" type="text" />
                                <label for="member_3_institution">Institution</label>
                                <input name="member_3_institution" type="text"  />
                                <label for="member_3_department">Department</label>
                                <input name="member_3_department" type="text"  />
                                <label for="member_3_name">Name</label>
                                <input name="member_3_name" type="text"  />
                                <label for="member_3_email">Email</label>
                                <input name="member_3_email" type="text"  />
                                <label for="member_3_phone">Phone</label>
                                <input name="member_3_phone" type="text"  />
                                <label for="member_3_address">Address</label>
                                <textarea name="member_3_address" row="4" col="5"></textarea>
                                <label for="member_3_photo">Photo</label>
                                <input type="file" name="member_3_photo"/>
                                <p class="help-text" style="color: red">Size must be within 50KB</p>
                            </fieldset>
                        </div>
                        <div id="member4" class="tab-pane">
                            <fieldset>
                                <label>Student Id</label>
                                <input name="member_4_student_id" type="text" />
                                <label for="member_4_institution">Institution</label>
                                <input name="member_4_institution" type="text"  />
                                <label for="member_4_department">Department</label>
                                <input name="member_4_department" type="text"  />
                                <label for="member_4_name">Name</label>
                                <input name="member_4_name" type="text"  />
                                <label for="member_4_email">Email</label>
                                <input name="member_4_email" type="text"  />
                                <label for="member_4_phone">Phone</label>
                                <input name="member_4_phone" type="text"  />
                                <label for="member_4_address">Address</label>
                                <textarea name="member_4_address" row="4" col="5"></textarea>
                                <label for="member_4_photo">Photo</label>
                                <input type="file" name="member_4_photo"/>
                                <p class="help-text" style="color: red">Size must be within 50KB</p>
                            </fieldset>
                        </div>
                        <div id="member5" class="tab-pane">
                            <fieldset>
                                <label>Student Id</label>
                                <input name="member_5_student_id" type="text" />
                                <label for="member_5_institution">Institution</label>
                                <input name="member_5_institution" type="text"  />
                                <label for="member_5_department">Department</label>
                                <input name="member_5_department" type="text"  />
                                <label for="member_5_name">Name</label>
                                <input name="member_5_name" type="text"  />
                                <label for="member_5_email">Email</label>
                                <input name="member_5_email" type="text"  />
                                <label for="member_5_phone">Phone</label>
                                <input name="member_5_phone" type="text"  />
                                <label for="member_5_address">Address</label>
                                <textarea name="member_5_address" row="4" col="5"></textarea>
                                <label for="member_5_photo">Photo</label>
                                <input type="file" name="member_5_photo"/>
                                <p class="help-text" style="color: red">Size must be within 50KB</p>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>  
    </div>
</div>