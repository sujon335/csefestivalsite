<br/>
<br/>
<br/>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span2">
        </div>
        <div class="span10">
            <strong><h2><?php echo $record['field']; ?></h2></strong>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Project Name</th>
                        <th>Project Category</th>
                    </tr>
                </thead>
                <?php unset($record['field']); ?>
                <tbody>
                    <?php foreach ($record as $row): ?>
                        <tr>
                            <td ajax="true" project_id="<?php echo $row['project_id']; ?>"><a href="#project_info" data-toggle="modal"><?php echo $row['project_name']; ?></a></td>
                            <td><?php echo $row['project_type']; ?></td>      
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <script type="text/javascript">
                $(function(){             
                    $('td[ajax="true"]').click(function(){           
                        var project_id = $(this).attr('project_id');
                        $.get("<?php echo base_url(); ?>index.php/project_show/project_info/"+project_id,function(data){
                            $('.modal-body').html(data) ;
                        })
                    });
                });
            </script>
        </div>
        <div id="project_info" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Project Information</h3>
            </div>
            <div class="modal-body">
                <p>Loading…</p>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>
    </div>
</div>