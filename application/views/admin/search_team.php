<br/>
<br/>
<br/>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3">
            <?php echo form_open('admin/search_team'); ?>
            <label for="contest">Track</label>
            <select name="contest">
                <?php foreach ($contests as $contest): ?>
                    <option value="<?php echo $contest['field']; ?>"><?php echo $contest['field']; ?></option>
                <?php endforeach; ?>
                </select>
                <label for="team_name">Team Name</label>
                <input type="text" name="team_name"/>
                <input type="submit"  value="SUBMIT" class="btn btn-primary"/>
                <a class="btn btn-danger" href="<?php echo base_url(); ?>index.php/admin/manage_pages">Cancel</a>
            <?php echo form_close(); ?>
                </div>
                <div class="span9">
            <?php if (is_array($team) & (count($team)>0)): ?>
            <?php
                        if ($field != 'project_show') {
                            $this->load->view('bsadd/registeredteams', array(
                                'record' => array(
                                    $team,
                                    'field' => $field,
                                ),
                            ));
                        } else {

                            $this->load->view('project/registeredteams', array(
                                'record' => array(
                                    $team,
                                    'field' => $field,
                                ),
                            ));
                        }
            ?>
            <?php else: ?>
                    <div class="aler alert-error larger">No Result Found</div>
            <?php endif; ?>
        </div>
    </div>
</div>