-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2013 at 10:51 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `puzzle`
--

-- --------------------------------------------------------

--
-- Table structure for table `csefest2013_admin_login`
--

CREATE TABLE IF NOT EXISTS `csefest2013_admin_login` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `csefest2013_log`
--

CREATE TABLE IF NOT EXISTS `csefest2013_log` (
  `lid` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `puzzle_serial` int(11) NOT NULL,
  `given_answer` text NOT NULL,
  `answer_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`lid`),
  KEY `log_ibfk_1` (`uid`),
  KEY `log_ibfk_2` (`puzzle_serial`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=72 ;

-- --------------------------------------------------------

--
-- Table structure for table `csefest2013_progress`
--

CREATE TABLE IF NOT EXISTS `csefest2013_progress` (
  `uid` int(11) NOT NULL,
  `current_puzzle_serial` int(11) NOT NULL DEFAULT '1',
  `last_correct_answer` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `finished` tinyint(1) DEFAULT '0',
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `csefest2013_puzzle`
--

CREATE TABLE IF NOT EXISTS `csefest2013_puzzle` (
  `serial` int(11) NOT NULL,
  `photo` text NOT NULL,
  `hint` text NOT NULL,
  `ans` text NOT NULL,
  `logic` text NOT NULL,
  PRIMARY KEY (`serial`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `csefest2013_user`
--

CREATE TABLE IF NOT EXISTS `csefest2013_user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(50) NOT NULL,
  `student_id` varchar(11) NOT NULL,
  `uname` varchar(40) DEFAULT NULL,
  `ulevel` int(11) DEFAULT NULL,
  `uterm` int(11) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `institution` varchar(100) NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `student_id` (`student_id`),
  UNIQUE KEY `mail` (`mail`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
