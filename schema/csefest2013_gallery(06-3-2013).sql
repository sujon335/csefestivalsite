-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2013 at 11:16 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `csefest`
--

-- --------------------------------------------------------

--
-- Table structure for table `csefest2013_gallery`
--

CREATE TABLE IF NOT EXISTS `csefest2013_gallery` (
  `photo_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `caption` text NOT NULL,
  `file_path` text NOT NULL,
  PRIMARY KEY (`photo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `csefest2013_gallery`
--

INSERT INTO `csefest2013_gallery` (`photo_id`, `title`, `caption`, `file_path`) VALUES
(9, 'dfgdfg', '', 'assets/gallery/back_to_school_makeover.jpg'),
(10, 'dfgdfg', '', 'assets/gallery/fairylicious-bride-makeover-mou.jpg'),
(11, 'dfgdfg', '', 'assets/gallery/California_Girl_Makeover.jpg'),
(12, 'dfgdfg', '', 'assets/gallery/California_Girl_Makeover1.jpg'),
(13, 'uio', '', 'assets/gallery/goth_girl_makeover.jpg'),
(14, 'dfgdfg', '', 'assets/gallery/cozy-for-christmas-dress-up-gg4u.jpg'),
(15, 'dfgdfg', '', 'assets/gallery/americas_idol_prep.jpg'),
(16, 'dfgdfg', '', 'assets/gallery/57960_159696064047825_100000222967737_527414_98636_n.jpg'),
(17, 'dfgdfg', '', 'assets/gallery/58534_159703884047043_100000222967737_527576_5950583_n.jpg'),
(18, 'dfgdfg', '', 'assets/gallery/59814_159694720714626_100000222967737_527409_7803002_n.jpg'),
(19, 'uio', '', 'assets/gallery/59814_159694714047960_100000222967737_527407_2257502_n.jpg'),
(20, 'uio', '', 'assets/gallery/58534_159703910713707_100000222967737_527584_7358730_n.jpg'),
(21, 'uio', '', 'assets/gallery/57960_159696067381158_100000222967737_527415_713061_n.jpg'),
(22, 'uio', '', 'assets/gallery/Gram-1.jpg'),
(23, 'uio', '', 'assets/gallery/Gram-3.jpg'),
(24, 'uio', '', 'assets/gallery/Gram-7.jpg'),
(25, 'uio', '', 'assets/gallery/Gram-8.jpg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
